package main

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	otlp "github.com/bluexlab/otlp-util-go"
	"github.com/grpc-ecosystem/grpc-opentracing/go/otgrpc"
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
	"gitlab.com/xdlai0709/grpc-opentrace-example/pkg/proto/hello"
	"gitlab.com/xdlai0709/grpc-opentrace-example/pkg/proxy"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.opentelemetry.io/otel/attribute"
	"google.golang.org/grpc"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Please provide GRPC server address")
		return
	}
	// grpcServerAddr := fmt.Sprintf(`dns:///%s`, os.Args[1])
	grpcServerAddr := fmt.Sprintf(`kubernetes:///%s`, os.Args[1])
	// grpcServerAddr := "localhost:5000"

	proxyAddress := "localhost:9999"
	proxyServer, err := proxy.NewProxy(proxyAddress, grpcServerAddr)
	if err != nil {
		fmt.Printf("Fail to create proxy. %v", err)
		return
	}
	go func() { proxyServer.Serve() }()

	otlp.InitGlobalTracer(
		otlp.WithServiceName("hello-client"),
		otlp.WithEndPoint(os.Getenv("OTLP_END_POINT")),
		// otlp.WithEndPoint("localhost:4317"),
		otlp.WithInSecure(),
	)

	cfg := &config.Configuration{
		ServiceName: "hello-client",
		Sampler: &config.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans: true,
		},
	}

	tracer, closer, err := cfg.NewTracer()
	if err != nil {
		fmt.Printf("Fail to create Jaeger Tracer.  %v", err)
		return
	}
	defer closer.Close()
	opentracing.SetGlobalTracer(tracer)

	// opts := []grpc_retry.CallOption{
	// 	grpc_retry.WithBackoff(grpc_retry.BackoffLinear(100 * time.Millisecond)),
	// 	grpc_retry.WithMax(50),
	// }

	serviceConfig := `{
		"methodConfig": [
			{
				"name": [],
				"retryPolicy": {
					"maxAttempts": 5,
					"initialBackoff": "0.1s",
					"maxBackoff": "1s",
					"backoffMultiplier": 2,
					"retryableStatusCodes": ["RESOURCE_EXHAUSTED", "UNAVAILABLE"]
				}
			}
		],
		"loadBalancingConfig":[{ "round_robin": {} }]
	}`

	clientConn, err := grpc.Dial(
		proxyAddress,
		grpc.WithDefaultServiceConfig(serviceConfig),
		grpc.WithUnaryInterceptor(otgrpc.OpenTracingClientInterceptor(tracer)),
		grpc.WithInsecure(),
		// grpc.WithStreamInterceptor(grpc_retry.StreamClientInterceptor(opts...)),
		// grpc.WithUnaryInterceptor(grpc_retry.UnaryClientInterceptor(opts...)),
		grpc.WithUnaryInterceptor(otelgrpc.UnaryClientInterceptor()),
	)
	if err != nil {
		fmt.Printf("Fail to create grpc conn. %v", err)
		return
	}

	client := hello.NewHelloServiceClient(clientConn)
	api := API{client}
	http.HandleFunc("/hello", api.Hello)
	http.HandleFunc("/hello_stream", api.HelloStream)
	http.HandleFunc("/hello_error", api.HelloError)

	http.ListenAndServe(":5000", nil)
}

type API struct {
	grpcClient hello.HelloServiceClient
}

func funcA(ctx context.Context) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "func-A")
	defer span.Finish()

	ctx, otlpSpan := otlp.Start(ctx, "func-A")
	defer otlpSpan.End()

	time.Sleep(100 * time.Millisecond)
	funcB(ctx)
	time.Sleep(100 * time.Millisecond)
}

func funcB(ctx context.Context) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "func-B")
	defer span.Finish()

	ctx, otlpSpan := otlp.Start(ctx, "func-B")
	defer otlpSpan.End()

	time.Sleep(50 * time.Millisecond)
}

func (api API) Hello(w http.ResponseWriter, req *http.Request) {
	// Create a context and span. The context must be passed to grpc call.
	span, ctx := opentracing.StartSpanFromContext(context.Background(), "hello-rest")
	defer span.Finish()

	ctx, otlpSpan := otlp.Start(ctx, "hello-rest")
	defer otlpSpan.End()

	name := req.URL.Query().Get("name")
	nickName := req.URL.Query().Get("nickname")
	delayStr := req.URL.Query().Get("delay")
	delay, _ := strconv.Atoi(delayStr)

	span.SetTag("name", name)
	// span.SetBaggageItem("baggage_name", name)
	otlpSpan.SetAttributes(attribute.String("name", name))

	time.Sleep(20 * time.Millisecond)

	helloReq := hello.HelloRequest{
		Name:     name,
		NickName: nickName,
		Delay:    int32(delay),
	}
	resp, err := api.grpcClient.Hello(ctx, &helloReq)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Error %v", err)))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(resp.GetMessage()))
}

func (api API) HelloStream(w http.ResponseWriter, req *http.Request) {
	// Create a context and span. The context must be passed to grpc call.
	span, ctx := opentracing.StartSpanFromContext(context.Background(), "hello-rest")
	defer span.Finish()

	ctx, otlpSpan := otlp.Start(ctx, "hello-rest")
	defer otlpSpan.End()

	name := req.URL.Query().Get("name")
	nickName := req.URL.Query().Get("nickname")
	delayStr := req.URL.Query().Get("delay")
	delay, _ := strconv.Atoi(delayStr)
	loopStr := req.URL.Query().Get("loop")
	loop, _ := strconv.Atoi(loopStr)
	if loop == 0 {
		loop = 1
	}

	stream, err := api.grpcClient.HelloStream(ctx)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Error %v", err)))
		return
	}

	httpRespBody := bytes.Buffer{}

	for i := 0; i < loop; i++ {
		helloReq := hello.HelloRequest{
			Name:     name,
			NickName: nickName,
			Delay:    int32(delay),
		}
		if err := stream.Send(&helloReq); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("Error %v", err)))
			return
		}

		resp, err := stream.Recv()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("Error %v", err)))
			return
		}

		if i > 0 {
			httpRespBody.WriteString("\n")
		}
		httpRespBody.WriteString(resp.GetMessage())
	}

	w.WriteHeader(http.StatusOK)
	w.Write(httpRespBody.Bytes())

	stream.CloseSend()
}

func (api API) HelloError(w http.ResponseWriter, req *http.Request) {
	// Create a context and span. The context must be passed to grpc call.
	span, ctx := opentracing.StartSpanFromContext(context.Background(), "hello-rest")
	defer span.Finish()

	ctx, otlpSpan := otlp.Start(ctx, "hello-rest")
	defer otlpSpan.End()

	name := req.URL.Query().Get("name")
	nickName := req.URL.Query().Get("nickname")

	span.SetTag("name", name)
	// span.SetBaggageItem("baggage_name", name)
	otlpSpan.SetAttributes(attribute.String("name", name))

	time.Sleep(20 * time.Millisecond)

	helloReq := hello.HelloRequest{
		Name:     name,
		NickName: nickName,
	}
	resp, err := api.grpcClient.HelloError(ctx, &helloReq)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("Error %v", err)))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(resp.GetMessage()))
}
