package main

import (
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"

	otlp "github.com/bluexlab/otlp-util-go"
	"github.com/grpc-ecosystem/grpc-opentracing/go/otgrpc"
	"gitlab.com/xdlai0709/grpc-opentrace-example/pkg/proto/hello"
	"gitlab.com/xdlai0709/grpc-opentrace-example/pkg/server"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"

	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
)

func main() {
	otlp.InitGlobalTracer(
		otlp.WithServiceName("hello-server"),
		otlp.WithEndPoint(os.Getenv("OTLP_END_POINT")),
		otlp.WithInSecure(),
	)

	cfg := &config.Configuration{
		ServiceName: "hello-server",
		Sampler: &config.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans: true,
		},
	}

	tracer, closer, err := cfg.NewTracer()
	if err != nil {
		fmt.Printf("Fail to create Jaeger Tracer.  %v", err)
		return
	}
	defer closer.Close()
	opentracing.SetGlobalTracer(tracer)

	listen, err := net.Listen("tcp", ":5001")
	if err != nil {
		fmt.Printf("Fail to bind port 5001. %v", err)
		return
	}

	grpcServer := grpc.NewServer(
		grpc.ChainUnaryInterceptor(
			otelgrpc.UnaryServerInterceptor(),
			otgrpc.OpenTracingServerInterceptor(
				opentracing.GlobalTracer(),
			),
		),
	)
	hello.RegisterHelloServiceServer(grpcServer, server.Server{})

	// graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		// got signal, handle with it
		fmt.Println("Shutting down Coordinator gRPC server...")
		grpcServer.GracefulStop()
		grpcServer.Stop()
	}()

	grpcServer.Serve(listen)
}
