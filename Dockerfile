FROM golang:1.19.3-alpine3.16 as builder

# install upx dep
RUN apk add --no-cache \
    binutils \
    ca-certificates \
    curl \
    git \
    tzdata \
    upx 
#  && go get -u github.com/golang/dep/...

# setup the working directory
WORKDIR /go/src/gitlab.com/xdlai0709/grpc-opentrace-example

# install dependencies
# ADD Gopkg.toml Gopkg.toml
# ADD Gopkg.lock Gopkg.lock
# RUN dep ensure -vendor-only -v

# add source code
ADD . .
RUN go mod download

# build the client
RUN CGO_ENABLED=0 GOOS=`go env GOHOSTOS` GOARCH=`go env GOHOSTARCH` \
    go build -a -installsuffix cgo -o /go/bin/client ./cmd/client

# build the server
RUN CGO_ENABLED=0 GOOS=`go env GOHOSTOS` GOARCH=`go env GOHOSTARCH` \
    go build -a -installsuffix cgo -o /go/bin/server ./cmd/server

# FROM scratch
FROM alpine:3.16

ENV PATH /app:$PATH

WORKDIR /app
COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /usr/local/go/lib/time/zoneinfo.zip
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/bin/server /app/server
COPY --from=builder /go/bin/client /app/client

# add launch shell command
COPY docker-entrypoint.sh /usr/bin/

ENTRYPOINT ["docker-entrypoint.sh"]
