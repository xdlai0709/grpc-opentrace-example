package proxy_test

import (
	"context"
	"fmt"
	"net"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/xdlai0709/grpc-opentrace-example/pkg/proto/hello"
	"gitlab.com/xdlai0709/grpc-opentrace-example/pkg/proxy"
	"gitlab.com/xdlai0709/grpc-opentrace-example/pkg/server"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func TestProxy(t *testing.T) {
	realServer, err := PrepareRealServer(":6001")
	require.NoError(t, err)
	clientStub, err := PrepareClientStub("localhost:5001")
	require.NoError(t, err)

	proxy, err := proxy.NewProxy("localhost:5001", "localhost:6001")
	require.NoError(t, err)

	go func() { proxy.Serve() }()
	time.Sleep(1 * time.Second)

	request := &hello.HelloRequest{
		Name:  "hahaha",
		Delay: 1,
	}
	response, err := clientStub.Hello(context.Background(), request)
	require.NoError(t, err)
	fmt.Println(response)
	response, err = clientStub.Hello(context.Background(), request)
	require.NoError(t, err)
	fmt.Println(response)

	realServer.GracefulStop()
	proxy.Stop()
}

func PrepareRealServer(serverAddress string) (*grpc.Server, error) {
	listen, err := net.Listen("tcp", serverAddress)
	if err != nil {
		fmt.Printf("Fail to bind port 5001. %v", err)
		return nil, err
	}

	grpcServer := grpc.NewServer()
	hello.RegisterHelloServiceServer(grpcServer, server.Server{})

	go func() { grpcServer.Serve(listen) }()
	return grpcServer, nil
}

func PrepareClientStub(serverAddress string) (hello.HelloServiceClient, error) {
	clientConn, err := grpc.Dial(
		serverAddress,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return hello.NewHelloServiceClient(clientConn), nil
}
