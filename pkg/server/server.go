package server

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"time"

	otlp "github.com/bluexlab/otlp-util-go"
	"github.com/opentracing/opentracing-go"
	"gitlab.com/xdlai0709/grpc-opentrace-example/pkg/proto/hello"
)

type Server struct {
	hello.UnimplementedHelloServiceServer
}

func (s Server) Hello(ctx context.Context, req *hello.HelloRequest) (resp *hello.HelloResponse, err error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "server-hello")
	defer span.Finish()

	ctx, otlpSpan := otlp.Start(ctx, "server-hello")
	defer otlpSpan.End()

	fmt.Println("Hello is called.")
	if req.GetDelay() > 0 {
		time.Sleep(time.Duration(req.GetDelay()) * time.Second)
	}
	resp = &hello.HelloResponse{
		Message: fmt.Sprintf("Hello %s (%s) from %s.", req.GetName(), req.GetNickName(), GetLocalIP()),
	}
	return
}

func (Server) HelloStream(stream hello.HelloService_HelloStreamServer) error {
	span, ctx := opentracing.StartSpanFromContext(stream.Context(), "server-hello-stream")
	defer span.Finish()

	ctx, otlpSpan := otlp.Start(ctx, "server-hello-stream")
	defer otlpSpan.End()

	fmt.Println("HelloStream is called.")
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		if req.GetDelay() > 0 {
			time.Sleep(time.Duration(req.GetDelay()) * time.Second)
		}
		resp := &hello.HelloResponse{
			Message: fmt.Sprintf("Hello %s (%s) from %s.", req.GetName(), req.GetNickName(), GetLocalIP()),
		}
		if err := stream.Send(resp); err != nil {
			return err
		}
	}
}

func (Server) HelloError(ctx context.Context, req *hello.HelloRequest) (resp *hello.HelloResponse, err error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "server-hello-error")
	defer span.Finish()

	ctx, otlpSpan := otlp.Start(ctx, "server-hello-error")
	defer otlpSpan.End()

	fmt.Println("HelloError is called.")
	return nil, errors.New("kldfjlsjflksflsd")
}

func GetLocalIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, address := range addrs {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}
